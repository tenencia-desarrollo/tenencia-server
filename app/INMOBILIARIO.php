<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class INMOBILIARIO extends Model
{
    // Nombre de la tabla en MySQL.
	protected $table='inmobiliario';

	protected $fillable = array('nu_folio','nu_ofic','Exp_cat','Superficie_total','Vialidad','Calle','Lote','Manzana','Supermanzana','num_ext','num_int', 'num_munici');
	
}
