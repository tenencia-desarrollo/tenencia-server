<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PROPIETARIOS extends Model
{
    // Nombre de la tabla en MySQL.
	protected $table='propietarios';
	
	protected $fillable = array('Id_prop','Nom_tit','Patern','Matern', 'Tip_persona');


}
