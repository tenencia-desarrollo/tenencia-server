<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PROP_INMOB extends Model
{
    // Nombre de la tabla en MySQL.
	protected $table='prop_inmob';

	protected $fillable = array('Id_prop','nu_folio','nu_ofic','nu_porcen','tp_adj');
}
