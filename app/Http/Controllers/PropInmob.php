<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;



use App\INMOBILIARIO;
use App\PROP_INMOB;
use App\PROPIETARIOS;
use Illuminate\Database\Query\JoinClause;
 
class PropInmob extends Controller
{
    public function DatosDelInmueble($nu_folio,$nu_ofic)
    {   

        $existenciaFolioOficina = INMOBILIARIO::where('inmobiliario.nu_ofic','=', $nu_ofic) 
        ->where('inmobiliario.nu_folio', '=', $nu_folio)
        ->get();

        $vacio = count($existenciaFolioOficina);
        
        if($vacio!=0){

           //PONER UN IF QUE SI EL FOLIO Y OFICINA NO EXISTEN MANDAR UN ERROR SI SU LONGITUD ES 0 O ESTA VACIO

            $infoInmueble = DB::table('inmobiliario')
            ->join('prop_inmob', 'prop_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('propietarios', 'propietarios.Id_prop', '=', 'prop_inmob.Id_prop')
            ->join('municipios', 'municipios.Id_municipio', '=', 'inmobiliario.num_munici')
            ->join('estados', 'estados.Id_estado', '=', 'municipios.Id_estado')
            ->join('mov_inmob', 'mov_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('biactos', 'biactos.nu_folio', '=', 'inmobiliario.nu_folio')
            ->select('inmobiliario.Exp_cat', 'propietarios.Nom_tit', 'municipios.Desc_municipio', 'estados.nom_estado','mov_inmob.acto', 'biactos.Num_escritura', 'biactos.Num_notaria', 'biactos.Nom_notario')
            ->where('inmobiliario.nu_ofic','=', $nu_ofic) 
            ->where('inmobiliario.nu_folio', '=', $nu_folio)
            ->get();
            
            return response()->json($infoInmueble, 200);
        } else {
            return response()->json(['Error'=>'Folio no registrado en las oficinas, favor de registrarse en RPP'],404);
        }
    }



    public function VerificacionFolioTitular($nu_folio,$Nom_tit)
    {   

        $FolioTitular = DB::table('inmobiliario')
            ->join('prop_inmob', 'prop_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('propietarios', 'propietarios.Id_prop', '=', 'prop_inmob.Id_prop')
            ->where('inmobiliario.nu_folio', '=', $nu_folio)
            ->where('propietarios.Nom_tit','=', $Nom_tit)
            ->get();

        $vacio = count($FolioTitular);

        if($vacio!=0){
            return response()->json(['status'=>'El folio pertenece al titular'], 200);
        } else {
            return response()->json(['status'=>'El folio no pertenece al titular'],200);
        } 
    }
}























/*


INTENTOS PARA VER LA EXISTENCIA DEL FOLIO EN LA OFICINA

 $existenciaFolioOficina = DB::table('inmobiliario')
            ->select('nu_folio', 'nu_ofic')
            ->where('inmobiliario.nu_ofic','=', $nu_ofic) 
            ->where('inmobiliario.nu_folio', '=', $nu_folio);
            
        $existenciaFolioOficina = INMOBILIARIO::find($nu_folio, $nu_ofic);


INTENTOS DE ACCEDER A LA INFORMACIÓN

public function consultaRPP($nu_folio,$nu_ofic)
    {   

        
        if($nu_folio && $nu_ofic){

           
            $propietarios = DB::table('inmobiliario')
            ->join('prop_inmob', 'prop_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('propietarios', 'propietarios.Id_prop', '=', 'prop_inmob.Id_prop')
            ->join('municipios', 'municipios.Id_municipio', '=', 'inmobiliario.num_munici')
            ->join('estados', 'estados.Id_estado', '=', 'municipios.Id_estado')
            ->join('mov_inmob', 'mov_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('biactos', 'biactos.nu_folio', '=', 'inmobiliario.nu_folio')
            ->select('inmobiliario.Exp_cat', 'propietarios.Nom_tit', 'municipios.Desc_municipio', 'estados.nom_estado','mov_inmob.acto', 'biactos.Num_escritura', 'biactos.Num_notaria', 'biactos.Nom_notario')
            ->where('inmobiliario.nu_ofic', $nu_ofic AND 'inmobiliario.nu_folio', $nu_folio)
            ->get();

            return response()->json($propietarios, 200);
        } 
    }






public function consultaRPPpropietarios($nu_folio,$nu_ofic)
    {   
        $folioinmob = INMOBILIARIO::find('nu_folio');
        $oficinainmob = INMOBILIARIO::find('nu_ofic');
        $folioprop = PROP_INMOB::find('nu_folio');
        $oficinaprop = PROP_INMOB::find('nu_ofic');

        if(($folioinmob AND $oficinainmob == $folioprop AND $oficinaprop) == $nu_folio AND $nu_ofic ){

           
            $propietarios = DB::table('inmobiliario')
            ->join('prop_inmob', 'prop_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('propietarios', 'propietarios.Id_prop', '=', 'prop_inmob.Id_prop')
            ->select('inmobiliario.Exp_cat', 'propietarios.Nom_tit')
            ->where('inmobiliario.nu_folio', $nu_folio, 'inmobiliario.nu_ofic', $nu_ofic)
            ->get();

            return response()->json($propietarios, 200);
        } 
   

    }

    } 
    








     public function consultaRPP($nu_folio,$nu_ofic)
    {   
        $folioinmob = INMOBILIARIO::find('nu_folio');
        $oficinainmob = INMOBILIARIO::find('nu_ofic');
        $folioprop = PROP_INMOB::find('nu_folio');
        $oficinaprop = PROP_INMOB::find('nu_ofic');

        if($folioinmob AND $oficinainmob == $folioprop AND $oficinaprop ){

           
            $propietarios = DB::table('inmobiliario')
            ->join('prop_inmob', 'prop_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('propietarios', 'propietarios.Id_prop', '=', 'prop_inmob.Id_prop')
            ->join('municipios', 'municipios.Id_municipio', '=', 'inmobiliario.num_munici')
            ->join('estados', 'estados.Id_estado', '=', 'municipios.Id_estado')
            ->join('mov_inmob', 'mov_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
            ->join('biactos', 'biactos.nu_folio', '=', 'inmobiliario.nu_folio')
            ->select('inmobiliario.Exp_cat', 'propietarios.Nom_tit', 'municipios.Desc_municipio', 'estados.nom_estado','mov_inmob.acto', 'biactos.Num_escritura', 'biactos.Num_notaria', 'biactos.Nom_notario')
            //->where('inmobiliario.nu_folio', $nu_folio, 'inmobiliario.nu_ofic', $nu_ofic)
            ->get();

            return response()->json($propietarios, 200);
        } 
   

    }
    
    
    







    public function consultaRPP($nu_folio,$nu_ofic)
    {   
        if(INMOBILIARIO::where('nu_folio',$nu_folio)->exists() AND INMOBILIARIO::where('nu_ofic',$nu_ofic)->exists()){

            $propfolio = PROP_INMOB::where('nu_folio', $nu_folio);
            $propoficina = PROP_INMOB::where('nu_ofic', $nu_ofic);

            if($propfolio == $nu_folio AND $propoficina==$nu_ofic){
                $propietarios = DB::table('inmobiliario')
                ->join('prop_inmob', 'prop_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
                ->join('propietarios', 'propietarios.Id_prop', '=', 'prop_inmob.Id_prop')
                ->join('municipios', 'municipios.Id_municipio', '=', 'inmobiliario.num_munici')
                ->join('estados', 'estados.Id_estado', '=', 'municipios.Id_estado')
                ->join('mov_inmob', 'mov_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
                ->join('biactos', 'biactos.nu_folio', '=', 'inmobiliario.nu_folio')
                ->select('inmobiliario.Exp_cat', 'propietarios.Nom_tit', 'municipios.Desc_municipio', 'estados.nom_estado','mov_inmob.acto', 'biactos.Num_escritura', 'biactos.Num_notaria', 'biactos.Nom_notario')
                ->where('inmobiliario.nu_ofic', $propoficina AND 'inmobiliario.nu_folio', $propfolio)
                ->get();

                return response()->json($propietarios, 200);
            }
        } 
    }








    public function consultaRPP($nu_folio,$nu_ofic)
    {   
        if(INMOBILIARIO::where('nu_folio',$nu_folio)->get() == PROP_INMOB::where('nu_folio', $nu_folio)->exists() AND INMOBILIARIO::where('nu_ofic',$nu_ofic)->exists() == PROP_INMOB::where('nu_ofic', $nu_ofic)->exists()){

          
                $propietarios = DB::table('inmobiliario')
                ->join('prop_inmob', 'prop_inmob.nu_folio', '=', 'inmobiliario.nu_folio')
                ->join('propietarios', 'propietarios.Id_prop', '=', 'prop_inmob.Id_prop')
                ->select('inmobiliario.Exp_cat', 'propietarios.Nom_tit')
                ->where('inmobiliario.nu_folio', $nu_folio AND'inmobiliario.nu_ofic', $nu_ofic )
                ->get();

                return response()->json($propietarios, 200);
            }
        
    }








     public function consultaRPP($nu_folio,$nu_ofic)
    {   

        $propInmob = DB::table('prop_inmob')
                    ->join('propietarios', 'propietarios.Id_prop' , 'prop_inmob.Id_prop');

        $inmobiliario = DB::table('inmobiliario')
                ->joinSub($propInmob, 'po', function ($join) {
                    $join->on('inmobiliario.nu_folio', '=', 'po.nu_folio', 'AND' ,'inmobiliario.nu_ofic', '=', 'po.nu_ofic');
                })
                ->select('inmobiliario.Exp_cat','propietarios.Nom_tit')
                ->where( 'inmobiliario.nu_folio', $nu_folio AND 'inmobiliario.nu_ofic', $nu_ofic)
                ->get();

        return response()->json($inmobiliario, 200);
    }




    INTENTO DE VERIFICACION

      public function VerificacionFolioTitular($nu_folio,$Nom_tit)
    {   

        $existenciaFolio = INMOBILIARIO::where('inmobiliario.nu_ofic','=', $nu_folio)
        ->get(); 

        $existenciaTitular = PROPIETARIOS::where('propietarios.Nom_tit', '=', $Nom_tit)
        ->get();
        

        $vacioFolio = count($existenciaFolio);
        $vacioTitular = count($existenciaTitular);

        
        if($vacioFolio&&$vacioTitular!=0){  
            return response()->json(['ok'=>'El folio pertenece al titular'], 200);
        } else {
            return response()->json(['Error'=>'El folio no pertenece al titular'],404);
        } 
    }




    */