<?php

use Illuminate\Http\Request;

use App\INMOBILIARIO;
use APP\PROP_INMOB;
use App\PROPIETARIOS;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('datosinmueble/{nu_folio}/{nu_ofic}', 'PropInmob@DatosDelInmueble');

Route::get('titularfolio/{nu_folio}/{Nom_tit}', 'PropInmob@VerificacionFolioTitular');